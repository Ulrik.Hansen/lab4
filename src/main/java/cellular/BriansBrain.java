package cellular;

import datastructure.IGrid;
import datastructure.CellGrid;

import java.util.Arrays;
import java.util.List;
import java.util.Random;



public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }




    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row,column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		for (int i = 0; i < currentGeneration.numRows(); i++) {
			for (int j = 0; j < currentGeneration.numColumns(); j++) {
				nextGeneration.set(i,j,getNextCell(i,j));
			}
		}
		currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        if (getCellState(row,col).equals(CellState.ALIVE)) {
            return CellState.DYING;
        }
        if (getCellState(row,col).equals(CellState.DEAD)) {
            if (countNeighbors(row,col,CellState.ALIVE)==2) {
                return CellState.ALIVE;
            }
        }
        return CellState.DEAD;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

    private int countNeighbors(int row, int col, CellState state) {
        int counter = 0;
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				if ((i == 0) && (j == 0)) {
					continue;
				}
				if ((row + i < 0) || (col + j < 0) || (row + i >= numberOfRows()) || (col + j >= numberOfColumns())) {
					//Checks if neighbour is on the grid
					continue;
				}
				else if (getCellState(row+i,col+j).equals(CellState.ALIVE)) {
					counter++;
				}
			}
		}
		return counter;
    }

    
    
}
