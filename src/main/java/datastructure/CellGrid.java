package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int cols;
    
    CellState[][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        grid = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(rows,cols,CellState.ALIVE);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                newGrid.grid[i][j] = grid[i][j];
            }
        }
        return newGrid;
    }
    
}
